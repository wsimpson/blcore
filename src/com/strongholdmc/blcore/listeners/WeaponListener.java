package com.strongholdmc.blcore.listeners;

import java.sql.SQLException;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.strongholdmc.blcore.playerclasses.RPGWeapon;
import com.strongholdmc.blcore.storage.RPGPlayers;

public class WeaponListener implements Listener
{
    public void pickupItem(PlayerPickupItemEvent e)
    {
	try
	{
	    RPGWeapon.makeWeapons(RPGPlayers.getRPGPlayer(e.getPlayer()));
	}
	catch (SQLException e1)
	{
	    e1.printStackTrace();
	}
    }
}
