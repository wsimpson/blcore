package com.strongholdmc.blcore.main;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.strongholdmc.blcore.listeners.DropListeners;
import com.strongholdmc.blcore.listeners.PlayerListeners;
import com.strongholdmc.blcore.listeners.WeaponListener;
import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import com.strongholdmc.blcore.storage.DBUtil;
import com.strongholdmc.blcore.storage.RPGPlayers;
@SuppressWarnings("deprecation")
public class BlackLance extends JavaPlugin
{
    BukkitTask connect;
    static com.strongholdmc.blcore.removethisshit.MySQL MySQL;
    public static Plugin pl;
    static Connection c;
    public void onEnable()
    {
	PluginManager pm = Bukkit.getPluginManager();
	Bukkit.broadcastMessage("RELOADING TEST FIVE");
	pl=this;
	MySQL=new com.strongholdmc.blcore.removethisshit.MySQL((Plugin) this, "localhost", "3306", "RPG", "root", "enter11284");
	MySQL.openConnection();
	connect = new BukkitRunnable(){public void run(){if (!MySQL.checkConnection()){BlackLance.openConnect();}}}.runTaskTimer(this, 20, 60);
	c=MySQL.getConnection();
	try{ RPGPlayer.createRPGPlayers();}
	catch (SQLException e){e.printStackTrace();}
	File configFile = new File(this.getDataFolder(), "config.yml");
	if (!configFile.exists())
	{
	    this.saveDefaultConfig();
	}
	pm.registerEvents(new PlayerListeners(this, configFile), this);
	pm.registerEvents(new WeaponListener(), this);
	pm.registerEvents(new DropListeners(this), this);
    }
    public void onDisable()
    {
	Bukkit.getScheduler().cancelAllTasks();
	for(Player p : Bukkit.getOnlinePlayers())
	{
	    RPGPlayer rp = RPGPlayers.getRPGPlayer(p);
	    try{DBUtil.saveDataByID(rp.getUID(), rp);}
	    catch (SQLException e){e.printStackTrace(); }
	}
	MySQL.closeConnection();
    }
    public void saveIt()
    {
	this.saveConfig();
    }
    public static Connection getConnection(){return c;}
    public static void openConnect(){MySQL.openConnection();}
    public static Plugin getPlugin(){return pl;}
}